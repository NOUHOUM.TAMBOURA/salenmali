import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Accueil',  icon: 'ni-tv-2 text-primary', class: '' },  

    { path: '/commune', title: 'Commune',  icon:'ni-istanbul text-blue', class: '' },
    { path: '/mairie', title: 'Mairie',  icon:'ni-shop text-blue', class: '' },
   

    { path: '/user-profile', title: 'Marche',  icon:'ni-map-big text-blue', class: '' },
    { path: '/tables', title: 'Marchand',  icon:'ni-single-02 text-blue', class: '' },
    { path: '/dashboard', title: 'Recouvreur',  icon:'ni-single-02  text-blue', class: '' },
    // { path: '/register', title: 'Recouvreur',  icon:'ni-single-02  text-blue', class: '' },
    { path: '/login', title: 'Login',  icon:'ni-button-power text-danger', class: '' }


   
    
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public menuItems: any[];
  public isCollapsed = true;

  constructor(private router: Router) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });
  }
}
